
package Programa_2;

public class Lista2 {
protected Nodos2 inicio, fin; // Punteros para donde esta el inicio y el fin
public Lista2(){
inicio = null;
fin = null;
}
public void agregarAlInicio2(int elemento){
inicio = new Nodos2(elemento, inicio);
if (fin == null){
fin = inicio;
}
}
public void mostrarListaEnlazada2(){
Nodos2 recorrer = inicio;
System.out.println("");
while (recorrer != null){
System.out.print("["+ recorrer.dato+"--------------------------->");
recorrer = recorrer.siguiente;
}
System.out.println("");
}

}


package listas.enlazadas;

public class Lista {
protected Nodos inicio, fin; // Punteros para donde esta el inicio y el fin
public Lista(){
inicio = null;
fin = null;
}
public void agregarAlInicio(String elemento){
inicio = new Nodos(elemento, inicio);
if (fin == null){
fin = inicio;
}
}
public void mostrarListaEnlazada(){
Nodos recorrer = inicio;
System.out.println("");
while (recorrer != null){
System.out.print("["+ recorrer.dato+"] -->");
recorrer = recorrer.siguiente;
}
System.out.println("");
}  
}
